/*
 * odometrie.h
 *
 *  Created on: Nov 15, 2023
 *      Author: charles DAMAGGIO, Matthieu CROS
 */

#ifndef SRC_ODOMETRIE_H_
#define SRC_ODOMETRIE_H_
#include "math.h"

#define CONSTDEBUT   0
#define STOPFACTOR   0.5
#define MAXOBSTACLES 6

//consigne en vitesse
typedef struct _TV{
    float x;// = 0;
    float y;// = 0;
    float w;// = 0;
    float alpha;// = 0;
} Type_Vitesse;


//consigne en position
typedef struct {
	float x;// = 0;
    float y;// = 0;
    float alpha;// = 0;
} Type_Position;

typedef struct {
    float x;// = 0;
    float y;// = 0;
} Type_Accel;

typedef struct _OD{
	float Vitesse_Max;
	float Acceleration;
	float Vitesse_Angulaire_Max;
	float Acceleration_Angulaire;
	float Rayon_Roue_MM;
	float Distance_Roue_Centre_Robot_MM;
	float Diametre_Robot;
	float Zone_Securite;
	float Magie_Noire_A;
	float Magie_Noire_B;
	float Magie_Noire_C;
	float Magie_Noire_D;
}ODOMETRIE;

typedef enum {NOTFOUND, FOUND, FOUND_FORCED, BLOCKED} PathStatus;
typedef enum {NOTEXPLORED, EXPLORED, BLOCKING, FULLYBLOCKED} Obs_status;
typedef enum {FALSE, TRUE} bool;

/**
 * Bibliotheque :
 */
void ODOMETRIE_Init(ODOMETRIE *ODO);
void ODOMETRIE_change_Vitesse_Acceleration(float v, float a);
void ODOMETRIE_change_Distance_Rayon_Roue(float l, float r);
void odometrieLocale(float posDiff[], float *dx, float *dy, float *dw);
void calculCommandeAbsolu(float vx, float vy, float w, float alpha, float target[]);
void calculCommandeAbsolu2(float Vitesse, float Angle, float w, float alpha, float target[]);

void TV_calculCommandeAbsolu(Type_Vitesse consigne, float alpha, float target[]);
void changeObstacles(int nb, Type_Position *obs);
bool TP_isInside(Type_Position point);
bool isInside(float x, float y);
int calculCommandePosition(Type_Position commande_position, Type_Position position, Type_Vitesse *vitesse, float precision, float precisionAngle, float deltaT, float target[]);

/**
 * Definitions des fonctions
 */
void ODOMETRIE_Init(ODOMETRIE *ODO){
	ODO->Vitesse_Max = 1000.0;
	ODO->Acceleration = 200.0;
	ODO->Vitesse_Angulaire_Max = 1.0;
	ODO->Acceleration_Angulaire = 1.0;

	//A calculer sur place
	ODO->Rayon_Roue_MM = 30.0;
	ODO->Distance_Roue_Centre_Robot_MM = 175.0;
	ODO->Diametre_Robot = ODO->Distance_Roue_Centre_Robot_MM + 200.0;
	ODO->Zone_Securite = 5.0;

	ODO->Magie_Noire_A = 30.0*ODO->Distance_Roue_Centre_Robot_MM/(M_PI*ODO->Rayon_Roue_MM);
	ODO->Magie_Noire_B = 60.0/(M_2_PI*ODO->Rayon_Roue_MM);
	ODO->Magie_Noire_C = (M_PI*ODO->Rayon_Roue_MM);
	ODO->Magie_Noire_D = ODO->Magie_Noire_C/(2.0*ODO->Distance_Roue_Centre_Robot_MM);
	return;
}


#endif /* SRC_ODOMETRIE_H_ */
