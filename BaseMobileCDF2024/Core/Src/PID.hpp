/*
 * PID.hpp
 *
 *  Created on: Nov 15, 2023
 *      Author: charles DAMAGGIO, Matthieu CROS
 */

#ifndef SRC_PID_HPP_
#define SRC_PID_HPP_

typedef struct _SP{
	float kp;
	float kd;
	float ki;
	float umax;

	//Storage
	float Previous_Error;
	float Integral_Error;
}Simple_PID;

/**
 * Bibliotheque :
 */
void PID_Init(Simple_PID *PID);
void PID_Set_Parameters(Simple_PID *PID, float KpNew, float KdNew, float KiNew, float umaxNew);
void PID_Control_Eval(Simple_PID *PID,float value, float target, float deltaT, int *pwr, int *dir);

/**
 * Init_PID
 */
void PID_Init(Simple_PID *PID){
	PID->kp=1;
	PID->kd=0;
	PID->ki=0;
	PID->umax=1000;

	PID->Previous_Error = 0.0;
	PID->Integral_Error = 0.0;
	return;
}

void PID_Set_Parameters(Simple_PID *PID, float KpNew, float KdNew, float KiNew, float umaxNew){
	PID->kp=KpNew;
	PID->kd=KdNew;
	PID->ki=KiNew;
	PID->umax=umaxNew;
	return;
}
  // A function to compute the control signal
void PID_Control_Eval(Simple_PID *PID,float value, float target, float deltaT, int *pwr, int *dir){

	// error
    float e = target - value;

    // derivative
    float dedt = (e-PID->Previous_Error)/(deltaT);

    // integral
    PID->Integral_Error = PID->Integral_Error + e*deltaT;

    //anti windup
    if(PID->Integral_Error > PID->umax/PID->ki)
    	PID->Integral_Error = PID->umax/PID->ki;
    if(PID->Integral_Error < -PID->umax/PID->ki)
    	PID->Integral_Error = -PID->umax/PID->ki;

    // control signal
    float u = PID->kp*e + PID->kd*dedt + PID->ki*PID->Integral_Error;

    // motor power
    *pwr = (int) (u);
    if(*pwr < 0)*pwr = -(*pwr);
    if( *pwr > PID->umax ){
      *pwr = PID->umax;
    }

    // motor direction
    *dir = 1;
    if(u<0){
      *dir = -1;
    }

    // store previous error
    PID->Previous_Error = e;
    return;
  }

#endif /* SRC_PID_HPP_ */
