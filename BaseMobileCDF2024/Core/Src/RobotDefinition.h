/*
 * RobotDefinition.h
 *
 *  Created on: Nov 12, 2023
 *      Author: charles DAMAGGIO
 */

#ifndef SRC_ROBOTDEFINITION_H_
#define SRC_ROBOTDEFINITION_H_
#include "odometrie.h"

#define PWMMax 1000
#define NbMotor 4

typedef struct _Timerinfo{
	TIM_HandleTypeDef IdTimer;
	int IdChannel;
}TimerInfo;

typedef struct _motor{
	TimerInfo pwm;
	int Encodeurs[2];
	int OuverturePontH[2];
}MOTOR;

typedef struct _robot{
	long Previous_Time;// = 0;
	int Previous_Position[4];// = {0,0,0,0};
	volatile int Encodeurs_Position[4];// = {0,0,0,0};
//	float Target[NbMotor];
	Type_Position position;
	Type_Vitesse vitesse;
	MOTOR m[NbMotor];
}ROBOT;

typedef enum{Robot_Idle, Robot_Vitesse, Robot_Vitesse_Filtre, Robot_Goto_Position}Type_Commande;

typedef struct _Mode{
	Type_Commande Commande;// = Robot_Idle;
	char local;// = 0;
}Type_Mode;

/**
 * Bibliotheque :
 */
/**
 * Fonctions de Remplacement de STMDUINO
 * */
void ChangePowerMotor(MOTOR * M,int PowerPercent);

/**
 * Fonctions à partir des HAL
 * */
void ChangeDutyCycle(TIM_HandleTypeDef * IdTimer,int Channel, int Pulse);


/**
 * Definitions des fonctions :
 */
void ChangePowerMotor(MOTOR * M,int PowerPercent){
	int Pulse = PowerPercent * 10;
	if (PowerPercent > PWMMax)PowerPercent = PWMMax;
	ChangeDutyCycle(&(M->pwm.IdTimer),M->pwm.IdChannel,Pulse);
	return;
}
void ChangeDutyCycle(TIM_HandleTypeDef * IdTimer,int Channel, int Pulse){
	__HAL_TIM_SET_COMPARE(IdTimer,Channel,Pulse);
	return;
}
#endif /* SRC_ROBOTDEFINITION_H_ */
