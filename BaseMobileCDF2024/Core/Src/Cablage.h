/*
 * Cablage.h
 *
 *  Created on: Nov 15, 2023
 *      Author: charles DAMAGGIO
 */

#ifndef SRC_CABLAGE_H_
#define SRC_CABLAGE_H_

/**
 * MOTOR X
 * Nom_fonction_Pin (Position_Board) [Integration_Code]
*/

/**
 * MOTOR 1
 * EncodeurA (PC0)               [EXTI0]
 * EncodeurB (PC1)               [GPIO_INPUT]
 * In1       (PA1)               [GPIO_OUTPUT]
 * In2       (PA4)               [GPIO_OUTPUT]
 * PWM       (PB0 A3 Left n°34)  [TIM3 CH3]
 * */
/**
 * MOTOR 2
 * EncodeurA (PA9)                [EXTI9]
 * EncodeurB (PC7)                [GPIO_INPUT]
 * In1       (PA6)                [GPIO_OUTPUT]
 * In2       (PA7)                [GPIO_OUTPUT]
 * PWM       (PB6 D10 Right n°17) [TIM4 CH1]
* */

/**
 * MOTOR 3
 * EncodeurA (PB13)              [EXTI13]
 * EncodeurB (PB14)              [GPIO_INPUT]
 * In1       (PB2)               [GPIO_OUTPUT]
 * In2       (PB1)               [GPIO_OUTPUT]
 * PWM       (PA11  Right n°14)  [TIM1 CH4]
* */

/**
 * MOTOR 4
 * EncodeurA (PA10)              [EXTI10]
 * EncodeurB (PB3)               [GPIO_INPUT]
 * In1       (PB10)              [GPIO_OUTPUT]
 * In2       (PA8)               [GPIO_OUTPUT]
 * PWM       (PB4)               [TIM3 CH3]
* */

#endif /* SRC_CABLAGE_H_ */
