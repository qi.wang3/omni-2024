/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define EncodeurA_Moteur1_EXTI0_Pin GPIO_PIN_0
#define EncodeurA_Moteur1_EXTI0_GPIO_Port GPIOC
#define EncodeurB_Moteur1_Pin GPIO_PIN_1
#define EncodeurB_Moteur1_GPIO_Port GPIOC
#define In1_Moteur1_Pin GPIO_PIN_1
#define In1_Moteur1_GPIO_Port GPIOA
#define USART_TX_Pin GPIO_PIN_2
#define USART_TX_GPIO_Port GPIOA
#define USART_RX_Pin GPIO_PIN_3
#define USART_RX_GPIO_Port GPIOA
#define In2_Moteur1_Pin GPIO_PIN_4
#define In2_Moteur1_GPIO_Port GPIOA
#define LD2_Pin GPIO_PIN_5
#define LD2_GPIO_Port GPIOA
#define In1_Moteur2_Pin GPIO_PIN_6
#define In1_Moteur2_GPIO_Port GPIOA
#define In2_Moteur2_Pin GPIO_PIN_7
#define In2_Moteur2_GPIO_Port GPIOA
#define PWM_Moteur1_TIM3CH3_Pin GPIO_PIN_0
#define PWM_Moteur1_TIM3CH3_GPIO_Port GPIOB
#define In2_Moteur3_Pin GPIO_PIN_1
#define In2_Moteur3_GPIO_Port GPIOB
#define In1_Moteur3_Pin GPIO_PIN_2
#define In1_Moteur3_GPIO_Port GPIOB
#define In1_Moteur4_Pin GPIO_PIN_10
#define In1_Moteur4_GPIO_Port GPIOB
#define EncodeurA_Moteur3_EXTI13_Pin GPIO_PIN_13
#define EncodeurA_Moteur3_EXTI13_GPIO_Port GPIOB
#define EncodeurB_Moteur3_Pin GPIO_PIN_14
#define EncodeurB_Moteur3_GPIO_Port GPIOB
#define EncodeurB_Moteur2_Pin GPIO_PIN_7
#define EncodeurB_Moteur2_GPIO_Port GPIOC
#define In2_Moteur4_Pin GPIO_PIN_8
#define In2_Moteur4_GPIO_Port GPIOA
#define EncodeurA_Moteur2_EXTI9_Pin GPIO_PIN_9
#define EncodeurA_Moteur2_EXTI9_GPIO_Port GPIOA
#define EncodeurA_Moteur4_EXTI10_Pin GPIO_PIN_10
#define EncodeurA_Moteur4_EXTI10_GPIO_Port GPIOA
#define PWM_Moteur3_TIM1CH4_Pin GPIO_PIN_11
#define PWM_Moteur3_TIM1CH4_GPIO_Port GPIOA
#define TMS_Pin GPIO_PIN_13
#define TMS_GPIO_Port GPIOA
#define TCK_Pin GPIO_PIN_14
#define TCK_GPIO_Port GPIOA
#define EncodeurB_Moteur4_Pin GPIO_PIN_3
#define EncodeurB_Moteur4_GPIO_Port GPIOB
#define PWM_Moteur4_TIM3CH1_Pin GPIO_PIN_4
#define PWM_Moteur4_TIM3CH1_GPIO_Port GPIOB
#define PWM_Moteur2_TIM4CH1_Pin GPIO_PIN_6
#define PWM_Moteur2_TIM4CH1_GPIO_Port GPIOB

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
